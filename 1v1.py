import time
import numpy as np
from rlgym.envs import Match
from rlgym.gamelaunch.launch import LaunchPreference
from stable_baselines3 import PPO
from stable_baselines3.common.callbacks import CheckpointCallback
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common.vec_env import VecMonitor, VecNormalize, VecCheckNan
from stable_baselines3.ppo import MlpPolicy
from src.AI.state_setter import CustomStateSetter
from src.AI.rewards.rewards import Rewards
from rlgym.utils.obs_builders import AdvancedObs
from rlgym.utils.reward_functions.common_rewards import VelocityPlayerToBallReward
from rlgym.utils.state_setters import DefaultState
from rlgym.utils.terminal_conditions.common_conditions import NoTouchTimeoutCondition, TimeoutCondition, GoalScoredCondition
from rlgym_tools.sb3_utils import SB3MultipleInstanceEnv
from rlgym_tools.sb3_utils.sb3_multidiscrete_wrapper import SB3MultiDiscreteWrapper

if __name__ == '__main__':  # Required for multiprocessing
    half_life_seconds = 5   # Easier to conceptualize, after this many seconds the reward discount is 0.5
    frame_skip = 8
    physics_ticks_per_second = 120
    ep_len_seconds = 30
    max_steps = int(round(ep_len_seconds * physics_ticks_per_second / frame_skip))
    no_touch_condition = NoTouchTimeoutCondition(max_steps)

    fps = 120 / frame_skip
    gamma = np.exp(np.log(0.5) / (fps * half_life_seconds))
    print(f"fps={fps}, gamma={gamma})")


    def get_match():  # Need to use a function so that each instance can call it and produce their own objects
        return Match(
            team_size=2,  # 3v3 to get as many agents going as possible, will make results more noisy
            tick_skip=frame_skip,
            reward_function=Rewards(),  # Simple reward since example code
            self_play=True,
            terminal_conditions=[no_touch_condition],  # Some basic terminals
            obs_builder=AdvancedObs(),  # Not that advanced, good default
            state_setter=CustomStateSetter(),  # Resets to kickoff position
        )

    env = SB3MultipleInstanceEnv(get_match, num_instances=1, wait_time=30, launch_preference=LaunchPreference.EPIC)            # Start 2 instances, waiting 30 seconds between each
    env = SB3MultiDiscreteWrapper(env)                    # Convert action space to multidiscrete
    env = VecCheckNan(env)                                # Optional
    env = VecMonitor(env)                                 # Recommended, logs mean reward and ep_len to Tensorboard
    env = VecNormalize(env, norm_obs=False, gamma=gamma)  # Highly recommended, normalizes rewards

    env.reset()
    model = PPO.load("src/model/to-ball-v8-age450", env, custom_objects=dict(n_envs=env.num_envs))
    env.reset()  # Important when loading models, SB3 does not do this for you
    mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=1)
    # Hyperparameters presumably better than default; inspired by original PPO paper
    # model = PPO(
    #     MlpPolicy,
    #     env,
    #     n_epochs=32,                 # PPO calls for multiple epochs
    #     learning_rate=1e-5,          # Around this is fairly common for PPO
    #     ent_coef=0.01,               # From PPO Atari
    #     vf_coef=1.,                  # From PPO Atari
    #     gamma=gamma,                 # Gamma as calculated using half-life
    #     verbose=3,                   # Print out all the info as we're going
    #     batch_size=4096,             # Batch size as high as possible within reason
    #     n_steps=4096,                # Number of steps to perform before optimizing network
    #     tensorboard_log="src/other",  # `tensorboard --logdir out/logs` in terminal to see graphs
    #     device="auto"                # Uses GPU if available
    # )

    # Divide by num_envs (number of agents) because callback only increments every time all agents have taken a step
    # This saves to specified folder with a specified name
    version = 8
    total_age = 342
    

    while True:
        obs = env.reset()
        done = False
        steps = 0
        ep_reward = 0
        while not done:
            action, _state = model.predict(obs, deterministic=True)
            new_obs, reward, done, state = env.step(action)
            ep_reward += reward
            obs = new_obs

    while True:
        total_age += 1
        model = model.learn(total_timesteps=int(200000), tb_log_name="move-to-ball-version{}".format(version), reset_num_timesteps=False)
        model.save("src/model/to-ball-v{}-age{}".format(version, total_age))

    # Now, if one wants to load a trained model from a checkpoint, use this function
    # This will contain all the attributes of the original model
    # Any attribute can be overwritten by using the custom_objects parameter,
    # which includes n_envs (number of agents), which has to be overwritten to use a different amount
    model = PPO.load("policy/rl_model_1000002_steps.zip", env, custom_objects=dict(n_envs=env.num_envs))
    env.reset()  # Important when loading models, SB3 does not do this for you
    # Use reset_num_timesteps=False to keep going with same logger/checkpoints
    model.learn(100_000_000, callback=callback, reset_num_timesteps=False)