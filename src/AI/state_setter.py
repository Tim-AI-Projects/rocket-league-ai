from rlgym.utils.state_setters import StateSetter
from rlgym.utils.state_setters import StateWrapper
from rlgym.utils.common_values import BLUE_TEAM, ORANGE_TEAM, CEILING_Z
import numpy as np

class CustomStateSetter(StateSetter):
    def reset(self, state_wrapper: StateWrapper):
    
        # Set up our desired spawn location and orientation. Here, we will only change the yaw, leaving the remaining orientation values unchanged.
        desired_car_pos = [0,-4200,17] #x, y, z BLUE
        desired_yaw = np.pi/2


        SPAWN_BLUE_POS = [[0,-4200,17], [-2048, -2560, 17], [2048, -2560, 17], [-256, -3840, 17], [256, -3840, 17]]
        SPAWN_BLUE_YAW = [np.pi/2, 0.25 * np.pi, 0.75 * np.pi, 0.5 * np.pi, 0.5 * np.pi]
        SPAWN_ORANGE_POS = [[0,4200,17], [2048, 2560, 17], [-2048, 2560, 17], [256, 3840, 17], [-256, 3840, 17]]
        SPAWN_ORANGE_YAW = [np.pi/2 , -0.75 * np.pi, -0.25 * np.pi, -0.5 * np.pi, -0.5 * np.pi]
        
        blue_count = 0
        orange_count = 0

        # Loop over every car in the game.
        for car in state_wrapper.cars:
            if car.team_num == BLUE_TEAM:
                pos = SPAWN_BLUE_POS[blue_count]
                yaw = SPAWN_BLUE_YAW[blue_count]
                blue_count += 1
                
            elif car.team_num == ORANGE_TEAM:
                # We will invert values for the orange team so our state setter treats both teams in the same way.
                pos = [-1*coord for coord in SPAWN_ORANGE_POS[orange_count]]
                yaw = -1*SPAWN_ORANGE_YAW[orange_count]
                orange_count += 1
                
            # Now we just use the provided setters in the CarWrapper we are manipulating to set its state. Note that here we are unpacking the pos array to set the position of 
            # the car. This is merely for convenience, and we will set the x,y,z coordinates directly when we set the state of the ball in a moment.
            car.set_pos(*pos)
            car.set_rot(yaw=yaw)
            car.boost = 0.33
            
        # Now we will spawn the ball in the center of the field, floating in the air.
        # x_start_ball = np.random.randint(-1000,1000)
        # y_start_ball = np.random.randint(-1000,1000)
        random = np.random.randint(5)
        if random == 0:
            x_start_ball = -3000
            y_start_ball = 3000
        elif random == 1:
            x_start_ball = 3000
            y_start_ball = 3000 
        elif random == 2:
            x_start_ball = 2900
            y_start_ball = -4000 
        elif random == 3: 
            x_start_ball = -2900
            y_start_ball = -4000
        else:
            x_start_ball = 0
            y_start_ball = -4000
        
        state_wrapper.ball.set_pos(x=x_start_ball, y=y_start_ball, z=17) #CEILING_Z/2