from rlgym.utils.reward_functions import RewardFunction
from rlgym.utils import math
from rlgym.utils.gamestates import GameState, PlayerData, game_state
import numpy as np
import math as Math
from rlgym.utils.common_values import BALL_RADIUS, CAR_MAX_SPEED

class MoveTowardsBallReward():
    previous_player_position = None
    previous_ball_position = None

    def reset(self):
        self.previous_player_position = None
        self.previous_ball_position = None
        pass

    def get_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float:
        player_position = player.car_data.position # [100,100,17] x, y, z
        ball_position = state.ball.position # [100,100,17] x, y, z
        reward = 0

        new_distance = self.get_distance(player_position, ball_position) #new position

        if self.previous_ball_position is not None:
            old_distance = self.get_distance(self.previous_player_position, self.previous_ball_position) # old position
            difference = abs(old_distance - new_distance)

            if new_distance > old_distance:
                reward += - (difference / 800)
            elif new_distance == old_distance:
                reward += -0.2
        
        reward += (np.exp(-0.5 * new_distance / CAR_MAX_SPEED)) / 5

        if player.ball_touched:
            reward += (((state.ball.position[2] + BALL_RADIUS) / (2 * BALL_RADIUS)) ** 0.1) * 2 #self.aerial_weight ** is to the power of
        self.previous_player_position = player_position
        self.previous_ball_position = ball_position
        return reward

    def get_distance(self, player_position, ball_position):
        dist = np.linalg.norm(player_position - ball_position) - BALL_RADIUS
        return round(dist)
