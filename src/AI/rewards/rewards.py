from rlgym.utils.reward_functions import RewardFunction
from rlgym.utils import math
from rlgym.utils.gamestates import GameState, PlayerData, game_state
import numpy as np
import math as Math
from src.AI.rewards.player_velocity import VelocityReward
from src.AI.rewards.face_ball import FaceBallReward
from src.AI.rewards.move_to_ball import MoveTowardsBallReward
from src.AI.rewards.player_to_ball_velocity import VelocityPlayerToBallReward
from rlgym.utils.common_values import BALL_MAX_SPEED, BALL_RADIUS, CAR_MAX_SPEED


class Rewards(RewardFunction):
    def __init__(self):
        self.velocity_to_ball = VelocityPlayerToBallReward()
        self.move_to_ball = MoveTowardsBallReward()
        self.face_ball = FaceBallReward()
        self.velocity_player = VelocityReward()

    def reset(self, initial_state: GameState):
        self.move_to_ball.reset()
        pass

    def get_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float:
        reward = self.move_to_ball.get_reward(player, state, previous_action)
        reward += self.face_ball.get_reward(player, state, previous_action)
        reward += self.velocity_to_ball.get_reward(player, state, previous_action)
        ball_velocity = np.linalg.norm(state.ball.linear_velocity) / BALL_MAX_SPEED * (1 - 2 * False)
        reward += (ball_velocity * 10)
        #reward += self.velocity_player.get_reward(player, state, previous_action)
        # print(reward)

        # Touch needs to be higher and score needs to be higher than everyting combined
        # Reward for ball velocity 
        return reward

    def get_final_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float:
        return 0



# A RewardFunction is an object used by RLGym to compute the reward for each action every step. These methods are called by an RLGym environment during an episode.
# All reward functions must implement the following three methods:

# #Called every reset.
# reset(self, initial_state: GameState)

# #Called every step.
# get_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float

# #Called if the current state is terminal.
# get_final_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float