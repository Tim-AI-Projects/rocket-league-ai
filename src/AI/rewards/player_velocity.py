from rlgym.utils.reward_functions import RewardFunction
from rlgym.utils import math
from rlgym.utils.gamestates import GameState, PlayerData, game_state
import numpy as np
from rlgym.utils.common_values import BALL_RADIUS, CAR_MAX_SPEED

class VelocityReward():
    def get_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float:
        reward = np.linalg.norm(player.car_data.linear_velocity) / CAR_MAX_SPEED * (1 - 2 * False)
        return reward