from rlgym.utils import math
from rlgym.utils.common_values import CAR_MAX_SPEED
from rlgym.utils.gamestates import GameState, PlayerData
import numpy as np


class VelocityPlayerToBallReward():
    def get_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float:
        vel = player.car_data.linear_velocity
        pos_diff = state.ball.position - player.car_data.position
        
        norm_pos_diff = pos_diff / np.linalg.norm(pos_diff)
        vel /= CAR_MAX_SPEED
        reward = float(np.dot(norm_pos_diff, vel))
        return reward