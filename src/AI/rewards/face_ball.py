from rlgym.utils.reward_functions import RewardFunction
from rlgym.utils import math
from rlgym.utils.gamestates import GameState, PlayerData, game_state
import numpy as np
from rlgym.utils.common_values import BALL_RADIUS, CAR_MAX_SPEED

class FaceBallReward():
    def get_reward(self, player: PlayerData, state: GameState, previous_action: np.ndarray) -> float:
        pos_diff = state.ball.position - player.car_data.position
        norm_pos_diff = pos_diff / np.linalg.norm(pos_diff)
        reward =float(np.dot(player.car_data.forward(), norm_pos_diff))
        #0.7 / 5
        return (reward / 2)