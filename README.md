<div id="top"></div>


<!-- PROJECT SHIELDS 
[![Forks][forks-shield]][forks-url]
[![Issues][issues-shield]][issues-url]
-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/Tim-AI-Projects/rocket-league-ai/-/blob/main/README.md">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Rocket League AI</h3>

  <p align="center">
    Using reinforcement learning to make an AI play Rocket League!
    <br />
    <a href="https://gitlab.com/Tim-AI-Projects/rocket-league-ai/-/blob/main/README.md"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <!-- <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a> -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
    <img src="images/Rocket_league.jpg" alt="Logo">
</div>

### Rocket League AI
In Rocket League, players control cars and use them to hit a ball towards the other team's goal to score. Players' cars can jump to hit the ball while in mid-air, pick up speed boosts, use the added momentum to hit the ball, or ram into another player's car to destroy it.  

As a starting point for the AI, the goal is to make it hit and/or follow the ball. 

- Documentation about this project can be found here: [Rocket League process](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EYDcsFrKm0BLkz5tU3iKxIABpoQxcab6dw_L8CaSpLTlog?e=DswOjt)
- The personal challenge project plan can be found here: [Project plan](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EUkskXcqbR9Mn2nJwiRHCFAB7wu-7nJsoLH3onf03pCoFQ?e=dWOBTe)
- The other repositories can be found here: [Tim AI Projects](https://gitlab.com/Tim-AI-Projects)

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

This section lists any major frameworks/libraries used for the project.

* [rlgym](https://rlgym.github.io/)
* [Python](https://www.python.org/)
* [Tensorflow](https://www.tensorflow.org/)
* [Stable-Baselines3](https://stable-baselines3.readthedocs.io/en/master/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

All the things you need to setup before starting.

* A Windows 10 PC
* Install [Python >= 3.7](https://realpython.com/installing-python/) 
* [Rocket League (Both Steam and Epic are supported)](https://www.rocketleague.com/)
* [Bakkesmod](https://www.bakkesmod.com/)
* The RLGym plugin for Bakkesmod (It's installed automatically by pip: see install)
* [Read the RLGym docs](https://rlgym.github.io/)

### Installation

_Below is an example of how you can install and setup the app._

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Tim-AI-Projects/rocket-league-ai.git
   ```
2. Install pip packages
   ```sh
   pip install -r requirements.txt
   ```



## Install:
pip install -r requirements.txt


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

This shows an example of how to use the app.

To run the AI use (Bakkesmod needs to be turned on before running the following command): 
```sh
python rocket_league.py
```

### Ingame Commands:
* sv_soccar_gamespeed 100: fast gamespeed
* togglemenu controlleroverlay: see AI inputs

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTRIBUTING -->
## Contributing

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.com/Tim-AI-Projects](https://gitlab.com/Tim-AI-Projects)

<p align="right">(<a href="#top">back to top</a>)</p>



## To know
- https://rlgym.github.io/docs-page.html#introduction
- https://github.com/RLBot/RLBot/wiki/Useful-Game-Values


The make function comes with a number of optional parameters, which are explained in the Documentation section of the wiki. For convenience, all the make parameters and their types are listed here:

- game_speed: int = 100,
- tick_skip: int = 8,
- spawn_opponents: bool = True,
- self_play: bool = False,
- random_resets: bool = False,
- team_size: int = 1,
- terminal_conditions: List[object] = (TimeoutCondition(225), GoalScoredCondition()),
- reward_fn: object = DefaultReward(),
- obs_builder: object = DefaultObs(),
- path_to_rl: str = None,
- use_injector: bool = False

<p align="right">(<a href="#top">back to top</a>)</p>

### Save model every so often
Divide by num_envs (number of agents) because callback only increments every time all agents have taken a step
This saves to specified folder with a specified name


callback = CheckpointCallback(round(1_000_000 / env.num_envs), save_path="policy", name_prefix="rl_model")
model.learn(100_000_000, callback=callback)


Now, if one wants to load a trained model from a checkpoint, use this function
This will contain all the attributes of the original model
Any attribute can be overwritten by using the custom_objects parameter,
which includes n_envs (number of agents), which has to be overwritten to use a different amount


model = PPO.load("policy/rl_model_1000002_steps.zip", env, custom_objects=dict(n_envs=env.num_envs))


env.reset()  # Important when loading models, SB3 does not do this for you

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/forks/new
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/blob/main/LICENSE.txt
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/issues

