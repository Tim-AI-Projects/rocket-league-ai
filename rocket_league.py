import numpy as np
import rlgym
import time
from rlgym.gamelaunch.launch import LaunchPreference
from rlgym.utils.obs_builders.advanced_obs import AdvancedObs
from rlgym.utils.terminal_conditions.common_conditions import NoTouchTimeoutCondition
from stable_baselines3 import PPO
from stable_baselines3.ppo.policies import MlpPolicy
from src.AI.rewards.rewards import Rewards
from src.AI.state_setter import CustomStateSetter
from stable_baselines3.common.evaluation import evaluate_policy
from rlgym.utils.reward_functions.common_rewards.player_ball_rewards import VelocityPlayerToBallReward


half_life_seconds = 5   # Easier to conceptualize, after this many seconds the reward discount is 0.5
frame_skip = 8
physics_ticks_per_second = 120
ep_len_seconds = 20
max_steps = int(round(ep_len_seconds * physics_ticks_per_second / frame_skip))
no_touch_condition = NoTouchTimeoutCondition(max_steps)

fps = 120 / frame_skip
gamma = np.exp(np.log(0.5) / (fps * half_life_seconds))
print(f"fps={fps}, gamma={gamma})")

env = rlgym.make(reward_fn=Rewards(), obs_builder=AdvancedObs(), state_setter=CustomStateSetter(), terminal_conditions=[no_touch_condition], launch_preference=LaunchPreference.STEAM) 
#param launch_preference: Rocket League launch preference (rlgym.gamelaunch.LaunchPreference) or path to RocketLeague executable

# Initialize PPO from stable_baselines3
# model = PPO(
#     MlpPolicy, 
#     env, 
#     verbose=1, 
#     tensorboard_log="src/other", 
#     learning_rate=1e-5, 
#     n_epochs=32, 
#     batch_size=4096, 
#     n_steps=4096, 
#     ent_coef=0.01, 
#     vf_coef=1.,   
#     gamma=gamma,
#     device="auto") #Clean start 

obs = env.reset()
model = PPO.load("src/model/to-ball-v7-age214", env=env, device="auto") #load
obs = env.reset()
# # Evaluate the agent
mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=1)


total_age = 214
version = 7
best_mean_reward = 0
best_std_reward = 0
while True:
    #Train our agent!
    total_age += 1
    model = model.learn(total_timesteps=int(100000), tb_log_name="move-to-ball-version{}".format(version), reset_num_timesteps=False) #1e6 && 200000
    model.save("src/model/to-ball-v{}-age{}".format(version, total_age))

    # Evaluate the agent
    mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)
    if best_mean_reward < mean_reward:
        best_mean_reward = mean_reward
        best_age = total_age
    if best_std_reward < best_std_reward:
        best_std_reward = std_reward
    print(best_std_reward)
    print(best_mean_reward)
    
# # Evaluate the agent
# mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)
# model = model.learn(total_timesteps=int(100000), tb_log_name="move-to-ball-test", reset_num_timesteps=False) #1e6 && 200000

while True:
    obs = env.reset()
    done = False
    steps = 0
    ep_reward = 0
    t0 = time.time()
    while not done:
        action, _state = model.predict(obs, deterministic=True)
        new_obs, reward, done, state = env.step(action)
        print(reward)
        ep_reward += reward
        obs = new_obs
        steps += 1

    length = time.time() - t0
    print("Step time: {:1.5f} | Episode time: {:.2f} | Episode Reward: {:.2f}".format(length / steps, length, ep_reward))
